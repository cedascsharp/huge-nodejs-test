import {firestore} from 'firebase-admin'
import ApiError from "./ApiError"
import * as moment from 'moment'

export default class AdvertisementModel {
  static readonly DB: firestore.Firestore = firestore();
  static readonly COLLECTION: string = 'ads';

  //#region Properties

    protected _id!: string;
    protected _message!: string;
    protected _offerGraphicUrl!: string;
    protected _startDateTime!: moment.Moment;
    protected _endDateTime!: moment.Moment;
    protected _category!: string;

    get ID(): string { return this._id; }
    get Message(): string { return this._message; }
    get OfferGraphicUrl(): string { return this._offerGraphicUrl; }
    get StartDateTime(): moment.Moment { return this._startDateTime; }
    get EndDateTime(): moment.Moment { return this._endDateTime; }
    get Category(): string { return this._category; }

    get Properties(): {
      id: string,
      message: string,
      offerGraphicUrl: string,
      startDateTime: moment.Moment,
      endDateTime: moment.Moment,
      category: string
    } { return {
      id: this.ID,
      message: this.Message,
      offerGraphicUrl: this.OfferGraphicUrl,
      startDateTime: this.StartDateTime,
      endDateTime: this.EndDateTime,
      category: this.Category
    }; }

  //#endregion

  constructor(
    { id='', message, offerGraphicUrl, startDateTime, endDateTime, category }:
    { id: string, message: string, offerGraphicUrl: string, startDateTime: string, endDateTime: string, category: string}
    | firestore.DocumentData
  ) {
    this.Update(arguments[0]);
  }

  //#region Static Methods

    protected static getCollection(): firestore.CollectionReference {
      return AdvertisementModel.DB.collection(AdvertisementModel.COLLECTION);
    }

    public static async FindByID(id: string): Promise<AdvertisementModel> {
      const doc = await AdvertisementModel.getCollection().doc(id).get();
      if(doc.exists) {
        const data = doc.data();
        if(data) return new AdvertisementModel({ id, ...data });
      }
      throw new Error("Could not find advertisement with id "+id);
    }

    public static async FindAll(): Promise<AdvertisementModel[]> {
      const docRefs: firestore.DocumentReference[] = await AdvertisementModel.getCollection().listDocuments();
      const docs: firestore.DocumentSnapshot[] = await Promise.all(docRefs.map(doc => doc.get()));
      const validDocs: firestore.DocumentSnapshot[] = docs.filter(doc => doc.exists);
      const models: Array<AdvertisementModel> = [];
      validDocs.forEach(doc => {
        const data: firestore.DocumentData|undefined = doc.data();
        if(data) models.push(new AdvertisementModel({ id: doc.id, ...data }));
      });
      return models;
    }

    public static async Update(id: string, data: Object): Promise<AdvertisementModel> {
      const doc: AdvertisementModel|null = await AdvertisementModel.FindByID(id);
      if(!doc) throw new Error("Could not find advertisement with id "+id);
      doc.Update(data);
      return await doc.Save();
    }

  //#endregion

  //#region Methods

    protected dateFromString(text: string): moment.Moment {
      const date = moment(text, 'YYYY-MM-DDTHH:mm:ssZ');
      if(date) return date;
      throw new ApiError(500, 'Invalid date provided');
    }

    /**
     * Tries to save the model in the database
     * Throws on failure
     * @throws Error
     */
    public async Save(): Promise<AdvertisementModel> {
      const { id, ...data } = this.toJSON();
      let entryRef: firestore.DocumentReference;
      if(id) {
         await AdvertisementModel.getCollection().doc(id).update(data);
         entryRef = AdvertisementModel.getCollection().doc(id);
      }
      else entryRef = await AdvertisementModel.getCollection().add(data);
      const entry: firestore.DocumentSnapshot = await entryRef.get();
      if(entry.exists) {
        const savedData = entry.data();
        if(savedData) return new AdvertisementModel({ id: entry.id, ...savedData });
      }
      throw new ApiError(500, 'Unable to save advertisement to database');
    }

    public Update(data: {[k: string]: any}): void {
      // TODO: Implement value validation
      const { id='', message, offerGraphicUrl, startDateTime, endDateTime, category } = data;
      if(id !== '') this._id = id;
      this._message = message;
      this._offerGraphicUrl = offerGraphicUrl;
      this._startDateTime = this.dateFromString(startDateTime);
      this._endDateTime = this.dateFromString(endDateTime);
      this._category = category;
    }

    public async Delete(): Promise<void> {
      if(this.ID === '') throw new Error('Cannot delete entry: It was never saved to database');
      await AdvertisementModel.getCollection().doc(this.ID).delete();
    }

    public StartsIn(date: { from: string, to: string }): Boolean {
      const startsFrom = this.dateFromString(date.from);
      const startsTo = this.dateFromString(date.to);
      return this.StartDateTime.isBetween(startsFrom, startsTo, 'milliseconds', '[]');
    }

    public EndsIn(date: { from: string, to: string }): Boolean {
      const endsFrom = this.dateFromString(date.from);
      const endsTo = this.dateFromString(date.to);
      return this.EndDateTime.isBetween(endsFrom, endsTo, 'milliseconds', '[]');
    }

    public HasCategory(category: string): Boolean {
      return this.Category === category;
    }

    public toJSON() {
      const data: {[key: string]: any} = { ...this.Properties };
      Object.keys(this.Properties).forEach(k => {
        if(k.endsWith('DateTime')) data[k] = data[k].toISOString();
      });
      return data;
    }

  //#endregion
}