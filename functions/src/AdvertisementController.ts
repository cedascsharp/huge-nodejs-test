import { Request, Response } from 'express'
import AdvertisementModel from "./AdvertisementModel";

export default class AdvertisementController {
  /**
   * POST CRUD request handler
   * Creates an item in the database
   * @param request
   * @param response
   */
  public static async create(request: Request, response: Response ): Promise<void> {
    console.log(request.body);
    try {
      const { message, offerGraphicUrl, category, startDateTime, endDateTime } = request.body;
      const data = {message, offerGraphicUrl, category, startDateTime, endDateTime};
      const entry = new AdvertisementModel(data);
      const savedEntry = await entry.Save();
      response.json(savedEntry);
    }
    catch(err) {
      response.status(500).json({ error: err.message });
    }
  }

  /**
   * GET CRUD request handler
   * Retrieves an item by ID from the database
   * @param request
   * @param response
   */
  public static async get(request: Request, response: Response): Promise<void> {
    try {
      const entryID = request.params.id;
      if(!entryID) throw new Error('Advertisement ID is required!');
      const entry = await AdvertisementModel.FindByID(entryID);
      response.json(entry);
    }
    catch(err) {
      response.status(500).json({ error: err.message });
    }
  }

  /**
   * GET CRUD request handler
   * Retrieves an item by ID from the database
   * @param request
   * @param response
   */
  public static async update(request: Request, response: Response): Promise<void> {
    try {
      const entryID = request.params.id;
      if(!entryID) throw new Error('Advertisement ID is required!');
      const entry = await AdvertisementModel.Update(entryID, request.body);
      response.json(entry);
    }
    catch(err) {
      response.status(500).json({ error: err.message });
    }
  }

  public static async getWithFilters(request: Request, response: Response): Promise<void> {
    const { startsIn=undefined, endsIn=undefined, category=undefined } = request.body;
    try {
      let entries: AdvertisementModel[] = await AdvertisementModel.FindAll();
      if(startsIn) entries = entries.filter(entry => entry.StartsIn(startsIn));
      if(endsIn) entries = entries.filter(entry => entry.EndsIn(endsIn));
      if(category) entries = entries.filter(entry => entry.HasCategory(category));
      response.json(entries);
    }
    catch(err) {
      response.status(500).json({ error: err.message });
    }
  }

  public static async delete(request: Request, response: Response): Promise<void> {
    try {
      const entryID = request.params.id;
      if(!entryID) throw new Error('Advertisement ID is required!');
      const entry = await AdvertisementModel.FindByID(entryID);
      await entry.Delete();
      response.json({ success: true });
    }
    catch(err) {
      response.status(500).json({ error: err.message });
    }
  }
}