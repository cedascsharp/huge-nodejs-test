import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import * as bodyParser from "body-parser";

admin.initializeApp(functions.config().firebase);

import AdvertisementController from "./AdvertisementController";

const app = express();
const main = express();

main.use('/api/v1', app);
main.use(bodyParser.json());

export const webApi = functions.https.onRequest(main);

const prefix = '/advertisements';
app.get(prefix, AdvertisementController.getWithFilters);
app.post(prefix, AdvertisementController.getWithFilters);
app.post(prefix + '/new', AdvertisementController.create);
app.get(prefix + '/:id', AdvertisementController.get);
app.put(prefix + '/:id', AdvertisementController.update);
app.delete(prefix + '/:id', AdvertisementController.delete);