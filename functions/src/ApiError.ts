export default class ApiError extends Error {
  protected _code: number;
  public get Code(): number { return this._code; }

  constructor(code: number, message: string) {
    super(message);
    this._code = code;
  }
}