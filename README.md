# Huge NodeJS Test

This a a submission for the NodeJS test provided by Huge Inc.

## Explanation

This project is a service that allows managing advertisements over a Restful CRUD api.
It was built with technologies to support infinite scalability and availability. To minimize
the costs of running this service, it makes use of Google Firebase Functions, which allow to start
an instance only when code needs to run, and then shutdown when there is no incoming requests.


The service runs a NodeJS 8 server that redirects all traffic to the Firebase Functions. The only
function defined is the request router (express) when then hands it to the Request Controller.
The controller then works with the model to create, read, update and delete data.

## How to use

The service can be tested with my own deployment at: https://huge-nodejs-test-254723.firebaseapp.com

The API documentation is located in the file `documentation.html` (generated with swagger.io)

Direct access to the database will not be provided because you would need me providing my personal credentials,
so you will have to follow the bottom installation instructions to access your own database.

## Installation

#### Dependencies
This service makes uses of different dependencies which must be installed and configured before
it can run:

1. NodeJS >= 8 (confirmed working with v8 up to v12 on windows);
2. A google Firebase account (This service runs in the free tier);

It is recommended to install NVM in order to switch node version easily.

#### Setting up the modules

Once the dependencies are met, the project can be cloned and the modules installed:
```
git clone https:/gitlab.com/cedascsharp/huge-nodejs-test.git
cd huge-nodejs-test/functions
npm install
```

#### Setting up the project

In the [firebase console](https://console.firebase.google.com), create a project (or select an existing one).
In the left sidebar, select "Database" and create a native Firestore. Create a collection with whatever name is desired.
This guide will use `advertisements`;

Back in the git directory, navigate to `functions/src/AdvertisementModel.ts`.
Inside that file, update the line 7 that starts with `static readonly COLLECTION` and update the
value 'ads' to the name of the collection previously created:

```typescript
class AdvertisementModel {
  static readonly COLLECTION: string = 'advertisements'; // Modify this line
}
```

Another file needs to be updated, that's `functions/src/index.ts`.
It contains a variabled `prefix`. That variable should also be updated with the new collection name;

Then, connect firebase to your project with the firebase login command.

***Note*** firebase-tools should be installed globally with NPM for the `firebase` command to work.
```
npm i -g firebase-tools
firebase login
```

#### Deploying

Once configured and logged, the project can be easily deployed with:
```
firebase deploy
```

## Tests

A small amount of tests have been implemented in the `functions/src/__tests__` directory.
To run them, navigate to `functions` and run `npm test`.

***NOTE*** The tests are connected to my own database. The `apiUrl` and `apiEndpoint` must be changed
for those tests to reflect your own database.

There are more tests missing that could've been implemented, but based on the wording found in the
huge nodejs test assignment, I understood that tests were not the focus and omitted them.

Tests for different value format, injection, etc should be included to make the service more secure.

## A word about security

There is currently zero security measures implemented (other than deploying). This could be easily
fixed by adding firebase's authentication and encryption services. A whitelist of IPs could also help
a lot in this specific case. 

I normally prioritize implementation of OWASP conventions but due to the limit of time to build this
project, it was skipped in favor of expressive and complete code.